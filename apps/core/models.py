from django.db import models


class Message(models.Model):
    created = models.DateTimeField()
    int_id = models.CharField(max_length=32)
    log_string = models.TextField()
    address = models.CharField(max_length=150, blank=True, null=True)
    status = models.BooleanField(default=False)


class Log(models.Model):
    created = models.DateTimeField()
    int_id = models.CharField(max_length=32)
    log_string = models.TextField()
    address = models.CharField(max_length=150, blank=True, null=True)

