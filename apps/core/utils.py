from datetime import datetime
import re


def parse_log_item(log_string: str) -> dict:
    flag = log_string[37:39]
    has_flag = flag in ('<=', '=>', '->', '**', '==')
    timestamp = log_string[:19]
    log_obj = {
        "dt": datetime.strptime(timestamp + '+00:00',  '%Y-%m-%d %H:%M:%S%z'),
        "int_id": log_string[20:36],
        "log_string": log_string[20:],
        "flag": flag,
        "address": "",
    }

    if has_flag:
        result = re.search(
            r'[a-zA-Z0-9\_]+[\._]?[a-zA-Z0-9\_]+[@][a-zA-Z0-9\_]+[\._]?[a-zA-Z0-9\_]+\.\w{2,3}',
            log_string[39:].strip()
        )
        if result:
            log_obj["address"] = result.group(0)

    return log_obj