from django import forms


class UploadForm(forms.Form):
    file = forms.FileField()


class SearchForm(forms.Form):
    address = forms.CharField(
        max_length=255, label='email',
        widget=forms.TextInput(attrs={'placeholder': 'some@email.com'})
    )
    sort_by_int_id = forms.BooleanField(
        required=False,
        initial=False,
        label='Сортировка по Int_ID'
    )
    sort_by_date = forms.BooleanField(
        required=False,
        initial=False,
        label='Сортировка по Created'
    )