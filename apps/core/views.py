from django.shortcuts import render, redirect
from django.views import View
from .forms import UploadForm, SearchForm
from .utils import parse_log_item
from .models import Message, Log
import time
from django.conf import settings


class IndexView(View):
    template_name = 'index.html'

    def get(self, request):
        form = SearchForm(request.GET)
        form_error = None
        log_resp = []
        if form.is_valid():
            orders = []
            if form.cleaned_data['sort_by_int_id']:
                orders.append('int_id')
            if form.cleaned_data['sort_by_date']:
                orders.append('-created')
            if not orders:
                orders.append('id')
            log_resp = Message.objects.filter(
                address=form.cleaned_data['address']
            ).order_by(*orders)
        else:
            form_error = form.errors
        context = {
            "form": SearchForm(),
            'log_resp': log_resp[:100],
            "lor_resp_len": log_resp.count() if log_resp else 0,
            'errors': form_error,
        }
        return render(request, self.template_name, context)


class UploadFileView(View):
    template_name = 'file_upload.html'

    def get(self, request):
        upload_form = UploadForm()
        is_success = bool(request.GET.get('is_success', None))
        context = {"form": upload_form, "is_success": is_success}
        return render(request, self.template_name, context)

    def post(self, request):
        if request.FILES['file']:
            myfile = request.FILES['file']

            to_message = []
            to_log = []

            for line in myfile:
                log_item = parse_log_item(line.decode("utf-8").strip())
                if log_item["flag"] == "<=":
                    to_message.append(
                        Message(
                            created=log_item["dt"],
                            address=log_item["address"],
                            log_string=log_item["log_string"],
                            int_id= log_item["int_id"],
                        )
                    )
                else:
                    to_log.append(
                        Log(
                            created=log_item["dt"],
                            address=log_item["address"],
                            log_string=log_item["log_string"],
                            int_id=log_item["int_id"],
                        )
                    )
            if to_message:
                Message.objects.bulk_create(to_message)
            if to_log:
                Log.objects.bulk_create(to_log)
        return redirect('/upload/?is_success=True')
