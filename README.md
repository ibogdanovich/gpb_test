# Разбор файла почтового лога.

Django приложение, состоящее из двух страниц: загрузки файла лога и поиска по
загруженным логам.

## Запуск

```shell
docker build -t gpb_test .
docker run -p 8000:8000 --name some_name gpb_test
```

## Поиск по логам

Поиск производится по email среди записей, находящихся в таблице `messages`.
В качестве дополнительный параметров группировки могут использоваться поля
`created` и `internal_id` как по отдельности, так и совместно. Если фильтр не
задан, то сортировка выполняется по `id` записи.

## Формат логов

Каждая строка лога имеет следующий формат:

```
created internal_id flag metainfo 
```
В качестве флагов используются следующие обозначения:

- `<=` прибытие сообщения (в этом случае за флагом следует адрес отправителя)
- `=>` нормальная доставка сообщения
- `->` дополнительный адрес в той же доставке
- `**` доставка не удалась
- `==` доставка задержана (временная проблема)

Пример фрагмента логов

```
2012-02-13 14:39:22 1RwtJa-000AFB-07 => :blackhole: <tpxmuwr@somehost.ru> R=blackhole_router
2012-02-13 14:39:22 1RwtJa-000AFB-07 Completed
2012-02-13 14:39:22 1RookS-000Pg8-VO ** fwxvparobkymnbyemevz@london.com: retry timeout exceeded
2012-02-13 14:39:22 1QcGoJ-000DxQ-75 Completed
2012-02-13 14:39:22 1RwtJa-0009RI-2d <= tpxmuwr@somehost.ru H=mail.somehost.com [84.154.134.45] P=esmtp S=1289 id=120213143629.COM_FM_END.205359@whois.somehost.ru
2012-02-13 14:39:22 1RwtJa-000AFJ-3B <= <> R=1RookS-000Pg8-VO U=mailnull P=local S=3958
2012-02-13 14:39:22 1RwtJa-000AFJ-3B => :blackhole: <tpxmuwr@somehost.ru> R=blackhole_router
2012-02-13 14:39:22 1RwtJa-000AFJ-3B Completed

```